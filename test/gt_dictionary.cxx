/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack



//
// includes
//

#include "CxxUtils/checker_macros.h"
ATLAS_NO_CHECK_FILE_THREAD_SAFETY;

#include <AsgTesting/UnitTest.h>
#include <AsgTools/AsgToolConfig.h>
#include <ColumnarPrototype/ColumnarExampleTool.h>
#include <ColumnarPrototype/ColumnarLinkExampleTool.h>
#include <gtest/gtest.h>
#include <gtest/gtest-spi.h>

#include <TInterpreter.h>

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

//
// method implementations
//

namespace col
{
  namespace
  {
    /// \brief make a unique tool name to be used in unit tests
    std::string makeUniqueName ()
    {
      static std::atomic<unsigned> index = 0;
      return "unique" + std::to_string(++index);
    }
  }

  // FIX ME: figure out why this crashes.  it is likely what keeps me
  // from using the AsgToolConfig class.
  TEST (DictionaryTest, newComponentBase)
  {
    std::cout << __FILE__ << ":" << __LINE__ << ": " << std::endl;
    auto *comp = reinterpret_cast<col::ColumnBase*>
      (gInterpreter->Calc("new col::ColumnBase"));

    std::cout << __FILE__ << ":" << __LINE__ << ": " << comp << std::endl;
    delete comp;
    std::cout << __FILE__ << ":" << __LINE__ << ": " << std::endl;
  }

  TEST (DictionaryTest, newTool)
  {
    const std::string name = makeUniqueName();
    std::cout << __FILE__ << ":" << __LINE__ << ": " << std::endl;
    auto *tool = reinterpret_cast<col::ColumnarExampleTool*>
      (gInterpreter->Calc(("new col::ColumnarExampleTool (\"" + name + "\")").c_str()));

    std::cout << __FILE__ << ":" << __LINE__ << ": " << tool << std::endl;
    delete tool;
    std::cout << __FILE__ << ":" << __LINE__ << ": " << std::endl;
  }

  TEST (DictionaryTest, makeTool)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));
  }
}

ATLAS_GOOGLE_TEST_MAIN
