/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack



//
// includes
//

#include "CxxUtils/checker_macros.h"
ATLAS_NO_CHECK_FILE_THREAD_SAFETY;

#include <AsgTesting/UnitTest.h>
#include <AsgTools/AsgToolConfig.h>
#include <ColumnarPrototype/ColumnarDynExampleTool.h>
#include <ColumnarPrototype/ColumnarExampleTool.h>
#include <ColumnarPrototype/ColumnarLinkExampleTool.h>
#include <gtest/gtest.h>
#include <gtest/gtest-spi.h>

#include <TInterpreter.h>

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

//
// method implementations
//

namespace col
{
  namespace
  {
    /// \brief make a unique tool name to be used in unit tests
    std::string makeUniqueName ()
    {
      static std::atomic<unsigned> index = 0;
      return "unique" + std::to_string(++index);
    }
  }

  TEST (ColumnarMode1Test, makeTool)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));
  }

  TEST (ColumnarMode1Test, getColumn)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (1);
    tool->setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    eta.push_back (1);
    output.resize (pt.size(), 0.);
    tool->setColumn ("Muons_pt", pt.size(), pt.data());
    tool->setColumn ("Muons_eta", eta.size(), eta.data());
    tool->setColumn ("Muons_output", output.size(), output.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (offsets.size());
    tool->setColumn ("EventInfo", numEvents.size(), numEvents.data());
    tool->checkColumnsValid ();

    EXPECT_EQ (pt.size(), tool->getColumn<const float>("Muons_pt").first);
    EXPECT_EQ (pt.data(), tool->getColumn<const float>("Muons_pt").second);
    EXPECT_EQ (output.size(), tool->getColumn<float>("Muons_output").first);
    EXPECT_EQ (output.data(), tool->getColumn<float>("Muons_output").second);
  }

  TEST (ColumnarMode1Test, object)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (1);
    tool->setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    eta.push_back (1);
    output.resize (pt.size(), 0.);
    tool->setColumn ("Muons_pt", pt.size(), pt.data());
    tool->setColumn ("Muons_eta", eta.size(), eta.data());
    tool->setColumn ("Muons_output", output.size(), output.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (offsets.size());
    tool->setColumn ("EventInfo", numEvents.size(), numEvents.data());
    tool->checkColumnsValid ();
    tool->calculateObject (ObjectId<ObjectType::muon>(0));
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
  }



  TEST (ColumnarMode1Test, range)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    pt.push_back (20e5);
    eta.push_back (1);
    eta.push_back (2);
    output.resize (pt.size(), 0.);
    tool->setColumn ("Muons_pt", pt.size(), pt.data());
    tool->setColumn ("Muons_eta", eta.size(), eta.data());
    tool->setColumn ("Muons_output", output.size(), output.data());
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    tool->setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (offsets.size());
    tool->setColumn ("EventInfo", numEvents.size(), numEvents.data());
    tool->checkColumnsValid ();
    tool->calculateRange (ObjectRange<ObjectType::muon>(0, offsets.back()));
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
    EXPECT_FLOAT_EQ (20e5*cosh(2), output[1]);
  }



  TEST (ColumnarMode1Test, vector)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    pt.push_back (20e5);
    eta.push_back (1);
    eta.push_back (2);
    output.resize (pt.size(), 0.);
    tool->setColumn ("Muons_pt", pt.size(), pt.data());
    tool->setColumn ("Muons_eta", eta.size(), eta.data());
    tool->setColumn ("Muons_output", output.size(), output.data());
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    tool->setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (offsets.size());
    tool->setColumn ("EventInfo", numEvents.size(), numEvents.data());
    tool->checkColumnsValid ();
    tool->calculateVector (ObjectRange<ObjectType::muon>(0, offsets.back()));
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
    EXPECT_FLOAT_EQ (20e5*cosh(2), output[1]);
  }



  TEST (ColumnarMode1Test, execute)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    pt.push_back (20e5);
    eta.push_back (1);
    eta.push_back (2);
    output.resize (pt.size(), 0.);
    tool->setColumn ("Muons_pt", pt.size(), pt.data());
    tool->setColumn ("Muons_eta", eta.size(), eta.data());
    tool->setColumn ("Muons_output", output.size(), output.data());
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    tool->setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (offsets.size());
    tool->setColumn ("EventInfo", numEvents.size(), numEvents.data());
    tool->checkColumnsValid ();
    tool->executeRange ();
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
    EXPECT_FLOAT_EQ (20e5*cosh(2), output[1]);
  }



  TEST (ColumnarMode1Test, dyn_column)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarDynExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarDynExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    std::vector<float> input1, input2, output;
    input1.push_back (1);
    input1.push_back (2);
    input2.push_back (4);
    input2.push_back (3);
    output.resize (input1.size(), 0.);
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (input1.size());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (offsets.size());

    std::map<std::string, std::vector<float>*> float_columns;
    float_columns["Muons_input1"] = &input1;
    float_columns["Muons_inputOptional"] = &input2;
    float_columns["Muons_output"] = &output;
    std::map<std::string, std::vector<ColumnarOffsetType>*> offset_columns;
    offset_columns["Muons"] = &offsets;
    offset_columns["EventInfo"] = &numEvents;

    for (auto& column : tool->getColumnNames())
    {
      if (auto iter = float_columns.find (column);
          iter != float_columns.end())
        tool->setColumn (column, iter->second->size(), iter->second->data());
      else if (auto iter = offset_columns.find (column);
               iter != offset_columns.end())
        tool->setColumn (column, iter->second->size(), iter->second->data());
      else
        throw std::logic_error ("unknown column: " + column);
    }
    tool->checkColumnsValid ();

    tool->executeObjectLoop ();
    EXPECT_FLOAT_EQ (1, output[0]);
    EXPECT_FLOAT_EQ (2, output[1]);
  }



  TEST (ColumnarMode1Test, dyn_column2)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarDynExampleTool/" + name);
    ASSERT_SUCCESS (config.setProperty ("input2", "inputOptional"));
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarDynExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    std::vector<float> input1, input2, output;
    input1.push_back (1);
    input1.push_back (2);
    input2.push_back (4);
    input2.push_back (3);
    output.resize (input1.size(), 0.);
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (input1.size());
    std::vector<ColumnarOffsetType> numEvents;
    numEvents.push_back (offsets.size());

    std::map<std::string, std::vector<float>*> float_columns;
    float_columns["Muons_input1"] = &input1;
    float_columns["Muons_inputOptional"] = &input2;
    float_columns["Muons_output"] = &output;
    std::map<std::string, std::vector<ColumnarOffsetType>*> offset_columns;
    offset_columns["Muons"] = &offsets;
    offset_columns["EventInfo"] = &numEvents;

    for (auto& column : tool->getColumnNames())
    {
      if (auto iter = float_columns.find (column);
          iter != float_columns.end())
        tool->setColumn (column, iter->second->size(), iter->second->data());
      else if (auto iter = offset_columns.find (column);
               iter != offset_columns.end())
        tool->setColumn (column, iter->second->size(), iter->second->data());
      else
        throw std::logic_error ("unknown column: " + column);
    }
    tool->checkColumnsValid ();

    tool->executeObjectLoop ();
    EXPECT_FLOAT_EQ (5, output[0]);
    EXPECT_FLOAT_EQ (5, output[1]);
  }



  TEST (ColumnarMode1Test, element_link)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarLinkExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarLinkExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    std::vector<float> calEta;
    calEta.push_back (0);
    calEta.push_back (1);
    calEta.push_back (2);
    calEta.push_back (3);
    calEta.push_back (4);
    calEta.push_back (5);
    calEta.push_back (6);
    tool->setColumn ("egammaClusters_calEta", calEta.size(), calEta.data());
    std::vector<ColumnarOffsetType> caloClusterOffsets;
    caloClusterOffsets.push_back (0);
    caloClusterOffsets.push_back (calEta.size());
    tool->setColumn ("egammaClusters", caloClusterOffsets.size(), caloClusterOffsets.data());

    std::vector<float> pt, output;
    std::vector<std::size_t> clusterLinkOffset;
    std::vector<std::size_t> clusterLinkData;
    pt.push_back (10e5);
    pt.push_back (20e5);
    clusterLinkOffset.push_back (0);
    clusterLinkOffset.push_back (2);
    clusterLinkOffset.push_back (5);
    clusterLinkData.push_back (0);
    clusterLinkData.push_back (1);
    clusterLinkData.push_back (2);
    clusterLinkData.push_back (3);
    clusterLinkData.push_back (0);
    output.resize (pt.size(), 0.);
    tool->setColumn ("Electrons_pt", pt.size(), pt.data());
    tool->setColumn ("Electrons_caloClusterLinks_offset", clusterLinkOffset.size(), clusterLinkOffset.data());
    tool->setColumn ("Electrons_caloClusterLinks_data", clusterLinkData.size(), clusterLinkData.data());
    tool->setColumn ("Electrons_output", output.size(), output.data());
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    tool->setColumn ("Electrons", offsets.size(), offsets.data());
    tool->checkColumnsValid ();
    tool->calculateObject (ObjectId<ObjectType::electron>(0));
    tool->calculateObject (ObjectId<ObjectType::electron>(1));

    EXPECT_FLOAT_EQ (10e5*cosh(0), output[0]);
    EXPECT_FLOAT_EQ (20e5*cosh(2), output[1]);
  }
}

ATLAS_GOOGLE_TEST_MAIN
