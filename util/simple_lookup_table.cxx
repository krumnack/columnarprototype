/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include <xAODRootAccess/Init.h>
#include <AsgMessaging/MessageCheck.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/ElectronAuxContainer.h>
#include <TH2.h>
#include <TRandom.h>
#include <TFile.h>
#include <array>
#include <chrono>
#include <iostream>

class MyAxis final
{
  unsigned m_bins = 0;
  float m_low = 0;
  float m_high = 0;
  float m_scale = 0;
  float m_max = 0;

public:
  MyAxis (unsigned val_bins, float val_low, float val_high) noexcept
    : m_bins (val_bins), m_low (val_low), m_high (val_high),
      m_scale (m_bins / (m_high - m_low)), m_max (m_bins)
  {}

  unsigned lookup (float value) const noexcept
  {
    float result = (value - m_low) * m_scale;
    if (result < 0)
      return 0;
    if (result >= m_max)
      return m_bins-1;
    return result;
  }

  unsigned nbins () const noexcept
  {
    return m_bins;
  }
};

class MyAxisSingleOverflow final
{
  unsigned m_bins = 0;
  float m_low = 0;
  float m_high = 0;
  float m_scale = 0;
  float m_max = 0;

public:
  MyAxisSingleOverflow (unsigned val_bins, float val_low, float val_high) noexcept
    : m_bins (val_bins), m_low (val_low), m_high (val_high),
      m_scale (m_bins / (m_high - m_low)), m_max (m_bins)
  {}

  unsigned lookup (float value) const noexcept
  {
    float result = (value - m_low) * m_scale;
    if (result < 0 || result >= m_max)
      return 0;
    return result;
  }

  unsigned nbins () const noexcept
  {
    return m_bins;
  }
};
class MyAxisBranchless final
{
  unsigned m_bins = 0;
  float m_low = 0;
  float m_high = 0;
  float m_scale = 0;
  float m_max = 0;

public:
  MyAxisBranchless (unsigned val_bins, float val_low, float val_high) noexcept
    : m_bins (val_bins), m_low (val_low), m_high (val_high),
      m_scale (m_bins / (m_high - m_low)), m_max (m_bins)
  {}

  unsigned lookup (float value) const noexcept
  {
    float result = (value - m_low) * m_scale;
    return unsigned (result) * (result >= 0) * (result < m_max);
  }

  unsigned nbins () const noexcept
  {
    return m_bins;
  }
};

class MyAxisVar final
{
  std::vector<float> m_boundaries;
  unsigned m_nbins = 0;

public:
  MyAxisVar (std::size_t val_bins, const float *val_boundaries) noexcept
    : m_nbins (val_bins)
  {
    for (unsigned bin = 0; bin+1 < val_bins; ++ bin)
      m_boundaries.push_back (val_boundaries[bin]);
  }

  unsigned lookup (float value) const noexcept
  {
    return std::upper_bound (m_boundaries.begin(), m_boundaries.end(), value) - m_boundaries.begin();
  }

  unsigned nbins () const noexcept
  {
    return m_nbins;
  }
};

class MyAxisVarBranchless final
{
  std::vector<float> m_boundaries;
  unsigned m_nbins = 0;

public:
  MyAxisVarBranchless (std::size_t val_bins, const float *val_boundaries)
    : m_nbins (val_bins)
  {
    if (m_nbins > 30)
      throw std::runtime_error ("Too many bins");
    for (unsigned bin = 0; bin+1 < val_bins; ++ bin)
      m_boundaries.push_back (val_boundaries[bin]);
    while (m_boundaries.size() < 32)
      m_boundaries.push_back (m_boundaries.back());
  }

  unsigned lookup (float value) const noexcept
  {
    unsigned bin = 0;
    bin = (value >= m_boundaries[16]) * 16;
    bin += (value >= m_boundaries[bin+8]) * 8;
    bin += (value >= m_boundaries[bin+4]) * 4;
    bin += (value >= m_boundaries[bin+2]) * 2;
    bin += (value >= m_boundaries[bin+1]) * 1;
    bin *= (bin < m_nbins);
    return bin;
  }

  unsigned nbins () const noexcept
  {
    return m_nbins;
  }
};

template<typename Axis>
class MyLookup final
{
  Axis m_xaxis, m_yaxis;
  std::vector<float> m_bins;

public:

  MyLookup (const Axis& val_xaxis, const Axis& val_yaxis) noexcept
    : m_xaxis (val_xaxis), m_yaxis (val_yaxis)
  {
    const unsigned nbins = m_xaxis.nbins() * m_yaxis.nbins();
    m_bins.reserve (nbins);
    while (m_bins.size() < nbins)
      m_bins.push_back (gRandom->Gaus(0,1));
  }

  float get (float x, float y) const noexcept
  {
    return m_bins [m_xaxis.lookup (x) * m_yaxis.nbins() + m_yaxis.lookup (y)];
  }
};

class MyLookupTH2 final
{
  std::unique_ptr<TH2> m_hist;

public:

  MyLookupTH2 (std::unique_ptr<TH2> val_hist) noexcept
    : m_hist (std::move (val_hist))
  {
    m_hist->SetDirectory (nullptr);
  }

  float get (float x, float y) const noexcept
  {
    return m_hist->GetBinContent (m_hist->FindBin (x, y));
  }
};


std::vector<std::pair<std::string,float>> result_list;

template<typename T> void benchmark (std::string name, const T& lookup)
{
  const unsigned target = 1e6;
  std::vector<float> inputx, inputy, output;

  inputx.reserve (target);
  inputy.reserve (target);
  while (inputx.size() < target)
  {
    inputx.push_back (gRandom->Uniform (2));
    inputy.push_back (gRandom->Uniform (2));
  }
  output.resize (target);

  std::vector<float> result;

  for (unsigned iter = 0; iter != 11; ++ iter)
  {
    auto start = std::chrono::high_resolution_clock::now();

    for (unsigned iter = 0; iter != target; ++ iter)
      output[iter] = lookup.get (inputx[iter], inputy[iter]);

    auto stop = std::chrono::high_resolution_clock::now();

    result.push_back ((1e6*target)/((stop - start)/std::chrono::microseconds(1)));
  }
  std::sort (result.begin(), result.end());
  result_list.emplace_back (name, result[result.size()/2]);
  std::cout << name << " - entries/second: " << result[result.size()/2] << std::endl;
}


template<typename T> void benchmark_xaod (std::string name, const T& lookup)
{
  const unsigned target = 1e6;
  auto electrons = std::make_unique<xAOD::ElectronContainer>();
  auto electronsAux = std::make_unique<xAOD::ElectronAuxContainer>();
  electrons->setStore (electronsAux.get());

  {
    SG::AuxElement::Decorator<float> pt ("pt");
    SG::AuxElement::Decorator<float> eta ("eta");
    electrons->reserve (target);
    while (electrons->size() < target)
    {
      electrons->push_back (new xAOD::Electron);
      pt(*electrons->back()) = gRandom->Uniform (2);
      eta(*electrons->back()) = gRandom->Uniform (2);
    }
  }

  std::vector<float> result;

  SG::AuxElement::ConstAccessor<float> pt ("pt");
  SG::AuxElement::ConstAccessor<float> eta ("eta");
  SG::AuxElement::Decorator<float> output ("output");

  for (unsigned iter = 0; iter != 11; ++ iter)
  {
    auto start = std::chrono::high_resolution_clock::now();

    for (unsigned iter = 0; iter != target; ++ iter)
    {
      auto& electron = *(*electrons)[iter];
      output(electron) = lookup.get (pt(electron), eta(electron));
    }

    auto stop = std::chrono::high_resolution_clock::now();

    result.push_back ((1e6*target)/((stop - start)/std::chrono::microseconds(1)));
  }
  std::sort (result.begin(), result.end());
  result_list.emplace_back (name, result[result.size()/2]);
  std::cout << name << " - entries/second: " << result[result.size()/2] << std::endl;
}


int main ()
{
  using namespace asg::msgUserCode;
  ANA_CHECK_SET_TYPE (int);

  ANA_CHECK (xAOD::Init());

  std::vector<float> bins;
  for (unsigned index = 0; index != 21; ++ index)
    bins.push_back (index * 2. / 20 + index*index/1000);

  benchmark ("simple-no-overflow", MyLookup<MyAxis> ({20, 0., 2}, {20, 0, 2}));
  benchmark ("simple-fixed", MyLookup<MyAxis> ({20, 0.05, 1.95}, {20, 0.05, 1.95}));
  benchmark ("simple-branchless", MyLookup<MyAxisBranchless> ({20, 0.05, 1.95}, {20, 0.05, 1.95}));
  benchmark ("simple-single-overflow", MyLookup<MyAxisSingleOverflow> ({20, 0.05, 1.95}, {20, 0.05, 1.95}));
  benchmark ("simple-var", MyLookup<MyAxisVar> ({bins.size()-2, bins.data()+1}, {bins.size()-2, bins.data()+1}));
  benchmark ("simple-var-branchless", MyLookup<MyAxisVarBranchless> ({bins.size()-2, bins.data()+1}, {bins.size()-2, bins.data()+1}));
  benchmark ("TH2-no-overflow", MyLookupTH2 (std::make_unique<TH2F>("lookup", "lookup", 20, 0., 2, 20, 0, 2)));
  benchmark ("TH2-fixed", MyLookupTH2 (std::make_unique<TH2F>("lookup", "lookup", 20, 0.05, 1.95, 20, 0.05, 1.95)));
  benchmark ("TH2-var", MyLookupTH2 (std::make_unique<TH2F>("lookup", "lookup", bins.size()-1, bins.data(), bins.size()-1, bins.data())));
  benchmark_xaod ("xaod-simple-no-overflow", MyLookup<MyAxis> ({20, 0., 2}, {20, 0, 2}));
  benchmark_xaod ("xaod-simple-fixed", MyLookup<MyAxis> ({20, 0.05, 1.95}, {20, 0.05, 1.95}));
  benchmark_xaod ("xaod-simple-var", MyLookup<MyAxisVar> ({bins.size()-2, bins.data()+1}, {bins.size()-2, bins.data()+1}));
  benchmark_xaod ("xaod-simple-var-branchless", MyLookup<MyAxisVarBranchless> ({bins.size()-2, bins.data()+1}, {bins.size()-2, bins.data()+1}));
  benchmark_xaod ("xaod-TH2-no-overflow", MyLookupTH2 (std::make_unique<TH2F>("lookup", "lookup", 20, 0., 2, 20, 0, 2)));
  benchmark_xaod ("xaod-TH2-fixed", MyLookupTH2 (std::make_unique<TH2F>("lookup", "lookup", 20, 0.05, 1.95, 20, 0.05, 1.95)));
  benchmark_xaod ("xaod-TH2-var", MyLookupTH2 (std::make_unique<TH2F>("lookup", "lookup", bins.size()-1, bins.data(), bins.size()-1, bins.data())));

  auto file = std::make_unique<TFile> ("lookup_benchmark.root", "RECREATE");
  auto hist = new TH1F ("hist", nullptr, result_list.size(), 0, result_list.size());
  hist->GetYaxis()->SetTitle ("lookups/second");
  hist->SetStats (true);

  unsigned bin = 1;
  for (auto& result: result_list)
  {
    hist->SetBinContent (bin, result.second);
    hist->GetXaxis()->SetBinLabel (bin, result.first.c_str());
    bin += 1;
  }
  file->Write ();
  file->Close ();

  return 0;
}
