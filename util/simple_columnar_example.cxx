/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include <xAODRootAccess/Init.h>
#include <AsgMessaging/MessageCheck.h>
#include <ColumnarPrototype/ColumnarExampleTool.h>
#include <TFile.h>

int main ()
{
  using namespace asg::msgUserCode;
  ANA_CHECK_SET_TYPE (int);

  ANA_CHECK (xAOD::Init());

  const char *test_file = getenv ("ASG_TEST_FILE_MC");
  if (!test_file)
  {
    ANA_MSG_ERROR ("couldn't find test-file variable");
    return 1;
  }
  std::unique_ptr<TFile> file (TFile::Open (test_file, "READ"));
  if (file == nullptr)
  {
    ANA_MSG_ERROR ("couldn't open test-file");
    return 1;
  }

  const std::string toolName = "MyTool";
  // asg::AsgToolConfig config ("col::ColumnarExampleTool/" + toolName);
  // std::shared_ptr<void> cleanup;
  // ToolHandle<ColumnarExampleTool> tool;
  // ASSERT_SUCCESS (config.makeTool (tool, cleanup));
  auto tool = std::make_unique<col::ColumnarExampleTool>(toolName);

  std::vector<float> pt, eta, output;
  pt.push_back (10e5);
  pt.push_back (20e5);
  eta.push_back (1);
  eta.push_back (2);
  output.resize (pt.size(), 0.);
  tool->setColumn ("Muons_pt", pt.size(), pt.data());
  tool->setColumn ("Muons_eta", eta.size(), eta.data());
  tool->setColumn ("Muons_output", output.size(), output.data());
  std::vector<col::ColumnarOffsetType> offsets;
  offsets.push_back (0);
  offsets.push_back (pt.size());
  tool->setColumn ("Muons", offsets.size(), offsets.data());
  tool->checkColumnsValid ();
  tool->calculateVector (col::ObjectRange<col::ObjectType::muon>(0, offsets.back()));

  return 0;
}
