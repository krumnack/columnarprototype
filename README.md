# ColumnarPrototype

This project is a prototype for the proposed columnar triple-use tools and data handles for columnar analysis.  The goal is to be able to provide tools/algorithms that can work in Athena, EventLoop, as well as columnar analysis via uproot/awkward or RDataFrame.  A detailed presentation of the idea was given here: https://indico.cern.ch/event/1215753/

The abbreviated version of the proposal is this:  At its most basic we would switch all tools and algorithms to access all decorations on objects via special data handles, that among other things allow direct vectorized access to the underlying data.  This completely encapsulates the xAOD infrastructure and allows to bypass it completely via compile time settings to allow direct access to the columnar data.

It should be noted that the expectation is that most tools/algorithms will be very simple, i.e. a single object type, 2-4 variables for that object, and often just a simple lookup table.  This prototype can easily support that, but it is likely also overkill if that is the only use case.  However, there are a number of tools that the user will likely have to run on PHYSLITE (MET, OR, muon) which require more variables, combine multiple objects, and perform complex calculations.

## Getting started

To try out the prototype, check out the package and include it as part of a standard ATLAS CMake setup:
```
git clone https://:@gitlab.cern.ch:8443/krumnack/ColumnarPrototype.git
```

This adds a cmake configuration flag to switch between the two data access modes (more may be added in the future to study different behaviors).  By default it will use the (non-columnar) xAOD data access mode.  To switch over to the columnar mode, you have to set the following flag:
```
cmake -DCOLUMNAR_ACCESS_MODE=1 ...
```

There is essentially no documentation right now.  Please take a look at the `ColumnarExampleTool` for an example of how such a tool could look like, and at the tests in the `test` directory for how the calling code would look like.

## Current state

The following features are already implemented:
- an `ObjectId` that allows to refer to individual objects
- an `ObjectRange` that allows to refer to a (continuous) range of objects (same event or multiple in columnar mode)
- an `EventId` that allows to identify an individual event
- an `EventRange` that allows to identify a range of events in columnar mode, or a single event in xAOD mode
- vectorized access to individual variable when dealing with object ranges
- support for range-based for loops with object ranges
- a `ColumnHandle` that allows both read and write access to individual columns
- a `ReadObjectHandle` that allows to refer to pre-existing objects in the input file
- `ReadObjectHandle` maps to a ReadHandleKey in Athena for dependency tracking
- in columnar mode no xAOD libraries are linked.

The following features are missing:
- a `WriteObjectHandle` that allows to create new objects (e.g. MET)
- support for algorithms as well as tools, probably through a `ColumnarAlgorithm` base class
- python bindings that allow calling from uproot/awkward
- ability to query the list of variables from the tool
- the vectorized data access should likely use `std::span` instead of Eigen (waiting for C++20 support)
- support configurability of the data handles
- wrap Gaudi data handles when working in Athena

Should this get adopted, it would have to be integrated into the ATLAS release which would require more changes (and decisions):
- introduce a new `ColumnarAnalysis` build project that switches on the columnar data access
- split up the package into multiple packages, so that the parts that depend on xAOD types are separate packages from the main package, so as not to introduce unneeded package dependencies in Athena
- strip out the xAOD infrastructure from the `ColumnarAnalysis` build project
- if not already present, add uproot, awkward and pybind as externals to the project
- decide whether the columnar release should include the xAOD infrastructure.  they don't get used them in any form, but not having them would make it harder to mix columnar and non-columnar tools in a package.

Unsolved problems:
- how to handle in-file meta-data.  this may be as simple as passing in the meta-data trees through the same infrastructure, but details would have to be worked out.
- how to handle element links.  this will likely be required for MET and OR, maybe also for e/gamma and muons.  for the most part the use of element links should be minimized (e.g. by decorating the cluster eta on the electron/photon instead of referencing the cluster and reading it from there), but it is likely that this is not possible in all situations.
- how to reduce or hide the heavy templating the prototype currently has.
- how to seemlessly integrate this with the systematics handling in the CP algorithms.  this is hopefully as simple as providing a separate implementation for the systematics data handles, but the details would have to be worked out.
- how to handle the composition of tools (either with other tools or algorithms)