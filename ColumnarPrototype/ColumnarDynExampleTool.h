/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_COLUMNAR_DYN_EXAMPLE_TOOL_H
#define COLUMNAR_PROTOTYPE_COLUMNAR_DYN_EXAMPLE_TOOL_H

#include <AsgTools/AsgTool.h>
#include <AsgTools/PropertyWrapper.h>
#include <ColumnarPrototype/ColumnHandle.h>
#include <ColumnarPrototype/ColumnBase.h>
#include <ColumnarPrototype/ReadObjectHandle.h>
#include <optional>

namespace col
{
  class ColumnarDynExampleTool final
    : public asg::AsgTool,
      public ColumnBase
  {
  public:

    ColumnarDynExampleTool (const std::string& name);

    StatusCode initialize () override;

    /// do a calculation on a single object
    void calculateObject (ObjectId<ObjectType::muon> muon);

    /// call the above in an execute-style function
    void executeObjectLoop ();

    /// call the above in an algorithm-execute-style function
    void executeEventsLoop ();

    ReadObjectHandle<ObjectType::event> m_events {*this, "EventInfo"};
    ReadObjectHandle<ObjectType::muon> m_muons {*this, "Muons"};
    ColumnHandle<ObjectType::muon,const float> m_input1 {*this, "input1"};
    Gaudi::Property<std::string> m_input2Name {this, "input2", "", "input2 column name"};
    std::optional<ColumnHandle<ObjectType::muon,const float>> m_input2;
    ColumnHandle<ObjectType::muon,float> m_output {*this, "output"};
  };
}

#endif
