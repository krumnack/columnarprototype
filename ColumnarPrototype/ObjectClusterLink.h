/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_OBJECT_RANGE_VIEW_H
#define COLUMNAR_PROTOTYPE_OBJECT_RANGE_VIEW_H

#include <ColumnarPrototype/ObjectType.h>
#include <ColumnarPrototype/ObjectId.h>

namespace col
{
  template<ObjectType OT,unsigned CM=columnarAccessMode> class ObjectClusterLink;





  template<ObjectType OT> class ObjectClusterLink<OT,0u> final
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr unsigned CM = 0u;

    ObjectClusterLink (const std::vector<ElementLink<typename detail::ObjectTypeTraits<OT>::xAODContainer>> *val_container) noexcept
      : m_container (val_container)
    {}

    [[nodiscard]] const std::vector<ElementLink<typename detail::ObjectTypeTraits<OT>::xAODContainer>> *getContainer () const noexcept {
      return m_container;}

    [[nodiscard]] ColumnarOffsetType size () const noexcept {
      return m_container->size();}

    [[nodiscard]] ObjectId<OT,CM> operator[] (ColumnarOffsetType index) const noexcept {
      return ObjectId<OT,CM>(*m_container->at(index));}



    /// Private Members
    /// ===============
  private:

    const std::vector<ElementLink<typename detail::ObjectTypeTraits<OT>::xAODContainer>> *m_container = nullptr;
  };





  template<ObjectType OT> class ObjectClusterLink<OT,1u> final
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr unsigned CM = 1u;

    explicit ObjectClusterLink (ColumnarOffsetType val_size,
                                const ColumnarOffsetType *val_data) noexcept
      : m_size (val_size), m_data (val_data)
    {}

    [[nodiscard]] ColumnarOffsetType size () const noexcept {
      return m_size;}

    [[nodiscard]] ObjectId<OT,CM> operator[] (ColumnarOffsetType index) const noexcept {
      assert (index < m_size);
      return ObjectId<OT,CM> (m_data[index]);}



    /// Private Members
    /// ===============
  private:

    ColumnarOffsetType m_size = 0u;
    const ColumnarOffsetType *m_data = nullptr;
  };
}

#endif
