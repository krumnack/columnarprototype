/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/


#ifndef PHYS_NTUPLE_DUMPER_PHYS_NTUPLE_DUMPER_DICT_H
#define PHYS_NTUPLE_DUMPER_PHYS_NTUPLE_DUMPER_DICT_H

#include <ColumnarPrototype/ObjectType.h>

#include <ColumnarPrototype/ColumnBase.h>
#include <ColumnarPrototype/ColumnarDynExampleTool.h>
#include <ColumnarPrototype/ColumnarExampleTool.h>
#include <ColumnarPrototype/ColumnarLinkExampleTool.h>

#endif
