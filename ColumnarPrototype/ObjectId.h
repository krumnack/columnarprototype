/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_OBJECT_ID_H
#define COLUMNAR_PROTOTYPE_OBJECT_ID_H

#include <ColumnarPrototype/ObjectType.h>
#include <exception>

namespace col
{
  namespace detail
  {
    template<ObjectType OT,bool isMutable> struct ObjectIdTraits
    {
      using xAODObject = const typename detail::ObjectTypeTraits<OT>::xAODObject;
      using xAODContainer = const typename detail::ObjectTypeTraits<OT>::xAODContainer;
    };
    template<ObjectType OT> struct ObjectIdTraits<OT,true>
    {
      using xAODObject = typename detail::ObjectTypeTraits<OT>::xAODObject;
      using xAODContainer = typename detail::ObjectTypeTraits<OT>::xAODContainer;
    };
  }

  /// @brief a class representing a single object (electron, muons, etc.)
  template<ObjectType O,bool isMutable = false, unsigned CM=columnarAccessMode> class ObjectId;





  template<ObjectType O,bool isMutable> class ObjectId<O,isMutable,0> final
  {
    /// Common Public Members
    /// =====================
  public:

    using xAODObject = typename detail::ObjectIdTraits<O,isMutable>::xAODObject;

    ObjectId (xAODObject *val_object) noexcept
      : m_object (val_object)
    {}

    ObjectId (xAODObject& val_object) noexcept
      : m_object (&val_object)
    {}

    ObjectId (const ObjectId<O,true>& that) noexcept
      : m_object (that.getObject()) {}

    [[nodiscard]] xAODObject *getObject () const noexcept {
      return m_object;}



    /// Private Members
    /// ===============
  private:

    xAODObject *m_object = nullptr;
  };





  template<ObjectType O,bool isMutable> class ObjectId<O,isMutable,1> final
  {
    /// Common Public Members
    /// =====================
  public:

    using xAODObject = typename detail::ObjectIdTraits<O,isMutable>::xAODObject;

    ObjectId (xAODObject * /*val_object*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    ObjectId (xAODObject& /*val_object*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    ObjectId (const ObjectId<O,true>& that) noexcept
      : m_index (that.getIndex()) {}

    [[nodiscard]] xAODObject *getObject () const {
      throw std::logic_error ("can't call xAOD function in columnar mode");}



    /// Mode-Specific Public Members
    /// ============================
  public:

    explicit ObjectId (int val_index) noexcept
      : m_index (val_index)
    {}

    explicit ObjectId (unsigned val_index) noexcept
      : m_index (val_index)
    {}

    explicit ObjectId (std::size_t val_index) noexcept
      : m_index (val_index)
    {}

    [[nodiscard]] std::size_t getIndex () const noexcept {
      return m_index;}



    /// Private Members
    /// ===============
  private:

    std::size_t m_index = 0u;
  };





  using MuonId = ObjectId<ObjectType::muon>;
  using MuonUpdateId = ObjectId<ObjectType::muon,true>;
  using EventId = ObjectId<ObjectType::event>;
  using ElectronId = ObjectId<ObjectType::electron>;
  using ClusterId = ObjectId<ObjectType::cluster>;
}

#endif
