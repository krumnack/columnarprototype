/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_COLUMNAR_LINK_EXAMPLE_TOOL_H
#define COLUMNAR_PROTOTYPE_COLUMNAR_LINK_EXAMPLE_TOOL_H

#include <AsgTools/AsgTool.h>
#include <ColumnarPrototype/ColumnHandle.h>
#include <ColumnarPrototype/ColumnBase.h>
#include <ColumnarPrototype/ReadObjectHandle.h>
#include <ColumnarPrototype/ClusterLinkColumnHandle.h>

namespace col
{
  class ColumnarLinkExampleTool final
    : public asg::AsgTool,
      public ColumnBase
  {
  public:

    ColumnarLinkExampleTool (const std::string& name);

    StatusCode initialize () override;

    /// do a calculation on a single object
    void calculateObject (ObjectId<ObjectType::electron> electron);

    ReadObjectHandle<ObjectType::electron> m_electrons {*this, "Electrons"};
    ColumnHandle<ObjectType::electron,const float> m_pt {*this, "pt"};
    ColumnHandle<ObjectType::electron,float> m_output {*this, "output"};
    ReadObjectHandle<ObjectType::cluster> m_clusters {*this, "egammaClusters"};
    ColumnHandle<ObjectType::cluster,const float> m_calEta {*this, "calEta"};

    ClusterLinkColumnHandle<ObjectType::electron,ObjectType::cluster> m_cluster_links {*this, "caloClusterLinks"};
  };
}

#endif
