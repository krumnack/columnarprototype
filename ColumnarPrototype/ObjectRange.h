/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_OBJECT_RANGE_H
#define COLUMNAR_PROTOTYPE_OBJECT_RANGE_H

#include <ColumnarPrototype/ObjectType.h>
#include <exception>

namespace col
{
  /// @brief a class representing a continuous sequence of objects (a.k.a. a container)
  template<ObjectType OT,bool isMutable = false,unsigned CM=columnarAccessMode> class ObjectRange;

  template<ObjectType OT,bool isMutable,unsigned CM> class ObjectRangeIterator;





  template<ObjectType OT,bool isMutable> class ObjectRange<OT,isMutable,0> final
  {
    /// Common Public Members
    /// =====================
  public:

    using xAODContainer = typename detail::ObjectIdTraits<OT,isMutable>::xAODContainer;
    static constexpr unsigned CM = 0u;

    ObjectRangeIterator<OT,isMutable,CM> begin () const noexcept {
      return ObjectRangeIterator<OT,isMutable,CM> (this, m_beginIndex);}
    ObjectRangeIterator<OT,isMutable,CM> end () const noexcept {
      return ObjectRangeIterator<OT,isMutable,CM> (this, m_endIndex);}

    [[nodiscard]] std::size_t beginIndex () const noexcept {
      return m_beginIndex;}
    [[nodiscard]] std::size_t endIndex () const noexcept {
      return m_endIndex;}

    ObjectRange (xAODContainer *val_container) noexcept
      : m_container (val_container),
        m_beginIndex (0)
    {
      if constexpr (OT == ObjectType::event)
        m_endIndex = 1;
      else
        m_endIndex = m_container->size();
    }

    ObjectRange (xAODContainer *val_container, std::size_t val_beginIndex, std::size_t val_endIndex) noexcept
      : m_container (val_container),
        m_beginIndex (val_beginIndex), m_endIndex (val_endIndex)
    {}

    [[nodiscard]] xAODContainer *getContainer () const noexcept {
      return m_container;}


    [[nodiscard]] ObjectId<OT,isMutable,CM> operator [] (std::size_t index) const noexcept {
      if constexpr (OT == ObjectType::event)
        return ObjectId<OT,isMutable,CM> (*m_container);
      else
        return ObjectId<OT,isMutable,CM> ((*m_container)[index]);
    }



    /// Private Members
    /// ===============
  private:

    xAODContainer *m_container = nullptr;
    std::size_t m_beginIndex = 0u;
    std::size_t m_endIndex = 0u;
  };





  template<ObjectType OT,bool isMutable> class ObjectRange<OT,isMutable,1> final
  {
    /// Common Public Members
    /// =====================
  public:

    using xAODContainer = typename detail::ObjectIdTraits<OT,isMutable>::xAODContainer;
    static constexpr unsigned CM = 1u;

    ObjectRangeIterator<OT,isMutable,CM> begin () const noexcept {
      return ObjectRangeIterator<OT,isMutable,CM> (this, m_beginIndex);}
    ObjectRangeIterator<OT,isMutable,CM> end () const noexcept {
      return ObjectRangeIterator<OT,isMutable,CM> (this, m_endIndex);}

    [[nodiscard]] std::size_t beginIndex () const noexcept {
      return m_beginIndex;}
    [[nodiscard]] std::size_t endIndex () const noexcept {
      return m_endIndex;}

    ObjectRange (xAODContainer * /*val_container*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    ObjectRange (xAODContainer * /*val_container*/, std::size_t /*val_beginIndex*/, std::size_t /*val_endIndex*/)
    {
      throw std::logic_error ("can't call xAOD function in columnar mode");
    }

    [[nodiscard]] xAODContainer *getContainer () const {
      throw std::logic_error ("can't call xAOD function in columnar mode");}

    [[nodiscard]] ObjectId<OT,isMutable,CM> operator [] (std::size_t index) const noexcept {
      return ObjectId<OT,isMutable,CM> (index);
    }



    /// Mode-Specific Public Members
    /// ============================
  public:

    explicit ObjectRange (std::size_t val_beginIndex,
                          std::size_t val_endIndex) noexcept
      : m_beginIndex (val_beginIndex), m_endIndex (val_endIndex)
    {}



    /// Private Members
    /// ===============
  private:

    std::size_t m_beginIndex = 0u;
    std::size_t m_endIndex = 0u;
  };



  /// @brief an iterator over objects in an @ref ObjectRange
  ///
  /// This is primarily to allow the use of range-for for ObjectRange

  template<ObjectType OT,bool isMutable,unsigned CM> class ObjectRangeIterator final
  {
  public:

    ObjectRangeIterator (const ObjectRange<OT,isMutable,CM> *val_range,
                         std::size_t val_index) noexcept
      : m_range (val_range), m_index (val_index) {}

    ObjectId<OT,isMutable,CM> operator * () const noexcept {
      return (*m_range)[m_index];
    }

    ObjectRangeIterator<OT,isMutable,CM>& operator ++ () noexcept {
      ++ m_index; return *this;}

    bool operator == (const ObjectRangeIterator<OT,isMutable,CM>& that) const noexcept {
      return m_index == that.m_index;}
    bool operator != (const ObjectRangeIterator<OT,isMutable,CM>& that) const noexcept {
      return m_index != that.m_index;}

  private:
    const ObjectRange<OT,isMutable,CM> *m_range = nullptr;
    std::size_t m_index = 0u;
  };

  using EventRange = ObjectRange<ObjectType::event>;
  using ElectronRange = ObjectRange<ObjectType::electron>;
}

#endif
