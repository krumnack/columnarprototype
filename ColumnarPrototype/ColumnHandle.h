/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_COLUMN_HANDLE_H
#define COLUMNAR_PROTOTYPE_COLUMN_HANDLE_H

#include <AthContainers/AuxElement.h>
#include <ColumnarPrototype/ColumnBase.h>
#include <ColumnarPrototype/ObjectId.h>
#include <ColumnarPrototype/ObjectRange.h>
#include <ColumnarPrototype/ObjectType.h>
#include <ColumnarPrototype/ReadObjectHandle.h>

#include <Eigen/Dense>

namespace col
{
  namespace detail
  {
    template<typename T> struct ColumnTraits
    {
      using Accessor = SG::AuxElement::Decorator<T>;
      using Vector = Eigen::Map<Eigen::Matrix<T,Eigen::Dynamic,1>>;
    };

    template<typename T> struct ColumnTraits<const T>
    {
      using Accessor = SG::AuxElement::ConstAccessor<T>;
      using Vector = Eigen::Map<const Eigen::Matrix<T,Eigen::Dynamic,1>>;
    };
  }

  /// @brief a data handle for accessing a specific column
  template<ObjectType OT,typename CT,unsigned CM=columnarAccessMode> class ColumnHandle;



  template<ObjectType OT,typename CT> class ColumnHandle<OT,CT,0> final
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr unsigned CM = 0u;

    ColumnHandle (ColumnBaseImp<CM>& /*columnBase*/,
                  const std::string& name)
      : m_accessor(name)
    {
    }

    /// alias for [] to be more compatible with the xAOD interface
    template<bool isMutable>
    CT& operator () (ObjectId<OT,isMutable,CM> id) const noexcept
    {
      return (*this)[id];
    }

    template<bool isMutable>
    CT& operator [] (ObjectId<OT,isMutable,CM> id) const noexcept
    {
      return m_accessor(*id.getObject());
    }

    typename detail::ColumnTraits<CT>::Vector operator [] (ObjectRange<OT,false,CM> range) const noexcept
    {
      if (range.getContainer()->empty())
        return typename detail::ColumnTraits<CT>::Vector (nullptr, 0u);
      return typename detail::ColumnTraits<CT>::Vector (&m_accessor (*(*range.getContainer())[0]), range.getContainer()->size());
    }

    [[nodiscard]] bool isAvailable (ObjectId<OT,false,CM> id, bool /*fallbackValue*/ = true) const noexcept
    {
      return m_accessor.isAvailable (*id.getObject());
    }



    /// Private Members
    /// ===============
  private:

    typename detail::ColumnTraits<CT>::Accessor m_accessor;
  };





  template<ObjectType OT,typename CT> class ColumnHandle<OT,CT,1u> final
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr unsigned CM = 1u;

    ColumnHandle (ColumnBaseImp<CM>& columnBase,
                  const std::string& name)
    {
      columnBase.addColumn (columnBase.objectName(OT) + "_" + name, m_size, m_data, columnBase.objectName (OT));
    }

    /// alias for [] to be more compatible with the xAOD interface
    template<bool isMutable>
    CT& operator () (ObjectId<OT,isMutable,CM> id) const noexcept
    {
      return (*this)[id];
    }

    template<bool isMutable>
    CT& operator [] (ObjectId<OT,isMutable,CM> id) const noexcept
    {
      return m_data[id.getIndex()];
    }

    typename detail::ColumnTraits<CT>::Vector operator [] (ObjectRange<OT,false,CM> range) const noexcept
    {
      return typename detail::ColumnTraits<CT>::Vector (m_data+range.beginIndex(), range.endIndex()-range.beginIndex());
    }

    [[nodiscard]] bool isAvailable (ObjectId<OT,false,CM> /*id*/, bool fallbackValue = true) const noexcept
    {
      return fallbackValue;
    }



    /// Private Members
    /// ===============
  private:

    std::size_t m_size = 0u;
    CT *m_data = nullptr;
  };





  template<typename CT> using MuonAccessor  = ColumnHandle<ObjectType::muon,const CT>;
  template<typename CT> using MuonDecorator = ColumnHandle<ObjectType::muon,CT>;
  template<typename CT> using TrackAccessor  = ColumnHandle<ObjectType::track,const CT>;
  template<typename CT> using TrackDecorator = ColumnHandle<ObjectType::track,CT>;
  template<typename CT> using ElectronAccessor  = ColumnHandle<ObjectType::electron,const CT>;
  template<typename CT> using ElectronDecorator = ColumnHandle<ObjectType::electron,CT>;
  template<typename CT> using EventAccessor  = ColumnHandle<ObjectType::event,const CT>;
  template<typename CT> using EventDecorator = ColumnHandle<ObjectType::event,CT>;
}

#endif
