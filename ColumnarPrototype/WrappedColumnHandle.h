/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_WRAPPED_COLUMN_HANDLE_H
#define COLUMNAR_PROTOTYPE_WRAPPED_COLUMN_HANDLE_H

// #include <AthContainers/AuxElement.h>
// #include <ColumnarPrototype/ColumnBase.h>
#include <ColumnarPrototype/ColumnHandle.h>
#include <ColumnarPrototype/OptObjectId.h>
// #include <ColumnarPrototype/ObjectId.h>
// #include <ColumnarPrototype/ObjectRange.h>
// #include <ColumnarPrototype/ObjectType.h>
// #include <ColumnarPrototype/ReadObjectHandle.h>

// #include <Eigen/Dense>

namespace col
{
  namespace detail
  {
    template<ObjectType OT,typename Wrapper,bool isDecorator,unsigned CM> struct WrappedColumnTraits
    {
      using BaseColumn = ColumnHandle<OT,typename Wrapper::BaseType,CM>;
    };

    template<ObjectType OT,typename Wrapper,unsigned CM> struct WrappedColumnTraits<OT,Wrapper,false,CM>
    {
      using BaseColumn = ColumnHandle<OT,const typename Wrapper::BaseType,CM>;
    };
  }

  template<ObjectType LinkType,unsigned CM = columnarAccessMode> struct ElementLinkWrapper;
  template<ObjectType OT,typename Wrapper,bool isDecorator = false,
      unsigned CM = columnarAccessMode> class WrappedColumnHandle;



  template<ObjectType LinkType> struct ElementLinkWrapper<LinkType,0u> final
  {
    static constexpr unsigned CM = 0u;
    using WrappedType = OptObjectId<LinkType,CM>;
    using BaseType = ElementLink<typename detail::ObjectTypeTraits<LinkType>::xAODContainer>;
    WrappedType get (const BaseType& value) const noexcept
    {
      return WrappedType (*value);
    }
    void set (BaseType& target, WrappedType value) const noexcept
    {
      target = value.getObject();
    }
  };


  template<ObjectType LinkType> struct ElementLinkWrapper<LinkType,1u> final
  {
    static constexpr unsigned CM = 1u;
    using WrappedType = OptObjectId<LinkType,CM>;
    using BaseType = ColumnarOffsetType;
    WrappedType wrap (BaseType value) const noexcept
    {
      return WrappedType (value);
    }
    void set (BaseType& target, WrappedType value) const noexcept
    {
      target = value.getIndex();
    }
  };


  struct TrackChargeWrapper final
  {
    using BaseType = float;
    float get (float value) const noexcept
    {
      return value > 0 ? 1 : (value < 0 ? -1 : 0);
    }
  };





  template<ObjectType OT,typename Wrapper,bool isDecorator = false,
      unsigned CM = columnarAccessMode> class WrappedColumnHandle final
  {
  public:
    WrappedColumnHandle (ColumnBaseImp<CM>& columnBase,
                  const std::string& name, const Wrapper& wrapper = {})
      : m_base(columnBase, name), m_wrapper(wrapper)
    {}

    template<bool isMutable>
    decltype(auto) operator [] (ObjectId<OT,isMutable,CM> id) const noexcept
    {
      return m_wrapper.get (m_base[id]);
    }

    template<bool isMutable,typename T> void
    set (ObjectId<OT,isMutable,CM> id, T&& value) const noexcept
    {
      m_wrapper.set (m_base[id], std::forward<T>(value));
    }

  private:

    typename detail::WrappedColumnTraits<OT,Wrapper,isDecorator,CM>::BaseColumn m_base;
    Wrapper m_wrapper;
  };

  template<ObjectType OT,ObjectType LT>
  using SimpleLinkColumnHandle = WrappedColumnHandle<OT,ElementLinkWrapper<LT>,false>;
}

#endif
