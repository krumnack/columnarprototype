/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_COLUMNAR_EXAMPLE_TOOL_H
#define COLUMNAR_PROTOTYPE_COLUMNAR_EXAMPLE_TOOL_H

#include <AsgTools/AsgTool.h>
#include <ColumnarPrototype/ColumnHandle.h>
#include <ColumnarPrototype/ColumnBase.h>
#include <ColumnarPrototype/ReadObjectHandle.h>

namespace col
{
  class ColumnarExampleTool final
    : public asg::AsgTool,
      public ColumnBase
  {
  public:

    ColumnarExampleTool (const std::string& name);

    StatusCode initialize () override;

    /// do a calculation on a single object
    void calculateObject (ObjectId<ObjectType::muon> muon);

    /// do a calculation on a range of objects using a range-based for-loop
    void calculateRange (ObjectRange<ObjectType::muon> muon);

    /// do a calculation on a range of objects using vectorized data access
    void calculateVector (ObjectRange<ObjectType::muon> muons);

    /// call the above in an execute-style function
    void executeObjectLoop ();
    void executeRange ();
    void executeVector ();

    /// call the above in an algorithm-execute-style function
    void executeEventsLoop ();
    void executeEventsRange ();

    ReadObjectHandle<ObjectType::event> m_events {*this, "EventInfo"};
    ReadObjectHandle<ObjectType::muon> m_muons {*this, "Muons"};
    ColumnHandle<ObjectType::muon,const float> m_pt {*this, "pt"};
    ColumnHandle<ObjectType::muon,const float> m_eta {*this, "eta"};
    ColumnHandle<ObjectType::muon,float> m_output {*this, "output"};
  };
}

#endif
