/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_OBJECT_TYPE_H
#define COLUMNAR_PROTOTYPE_OBJECT_TYPE_H

#include <xAODEventInfo/EventInfo.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODCaloEvent/CaloClusterContainer.h>
#include <xAODCaloEvent/CaloCluster.h>
#include <xAODTracking/TrackParticleContainer.h>

namespace col
{
  // This checks that COLUMNAR_ACCESS_MODE is indeed defined, plus makes it
  // available for use with `if constexpr`.

  constexpr unsigned columnarAccessMode = COLUMNAR_ACCESS_MODE;

  enum class ObjectType
  {
    muon,
    electron,
    cluster,
    track,
    event
  };

  namespace detail
  {
    template<ObjectType> struct ObjectTypeTraits;

    template<> struct ObjectTypeTraits<ObjectType::muon> final
    {
      using xAODContainer = xAOD::MuonContainer;
      using xAODObject = xAOD::Muon;
      using xAODStoreType = xAOD::MuonContainer;
    };

    template<> struct ObjectTypeTraits<ObjectType::electron> final
    {
      using xAODContainer = xAOD::ElectronContainer;
      using xAODObject = xAOD::Electron;
      using xAODStoreType = xAOD::ElectronContainer;
    };

    template<> struct ObjectTypeTraits<ObjectType::cluster> final
    {
      using xAODContainer = xAOD::CaloClusterContainer;
      using xAODObject = xAOD::CaloCluster;
      using xAODStoreType = xAOD::CaloClusterContainer;
    };

    template<> struct ObjectTypeTraits<ObjectType::track> final
    {
      using xAODContainer = xAOD::TrackParticleContainer;
      using xAODObject = xAOD::TrackParticle;
      using xAODStoreType = xAOD::TrackParticleContainer;
    };

    template<> struct ObjectTypeTraits<ObjectType::event> final
    {
      // FIX ME: this should be void, but for now other values don't hurt
      using xAODContainer = xAOD::EventInfo;
      using xAODObject = xAOD::EventInfo;
      using xAODStoreType = xAOD::EventInfo;
    };
  }
}

#endif
