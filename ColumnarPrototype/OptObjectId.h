/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack

#ifndef COLUMNAR_PROTOTYPE_OPT_OBJECT_ID_H
#define COLUMNAR_PROTOTYPE_OPT_OBJECT_ID_H

#include <ColumnarPrototype/ObjectId.h>
#include <ColumnarPrototype/ReadObjectHandle.h>
#include <optional>

namespace col
{
  /// @brief an @ref ObjectId wrapper that allows to be optional in xAOD mode
  ///
  /// This is essentially just meant for EventId, which is often passed in as an
  /// optional parameter in xAOD mode.  However, this only works in xAOD mode,
  /// in columnar mode we always have to pass the EventId as there are multiple
  /// events in flight.

  template<ObjectType OT,unsigned CM = columnarAccessMode> class OptObjectId;





  template<ObjectType OT> class OptObjectId<OT,0> final
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr unsigned CM = 0u;

    OptObjectId () noexcept = default;

    OptObjectId (const typename detail::ObjectTypeTraits<OT>::xAODObject *val_object) noexcept
    {
      if (val_object)
        m_id.emplace (val_object);
    }

    OptObjectId (const typename detail::ObjectTypeTraits<OT>::xAODObject& val_object) noexcept
      : m_id (val_object)
    {}

    [[nodiscard]] operator bool () const noexcept {
      return m_id.has_value();}

    [[nodiscard]] bool has_value () const noexcept {
      return m_id.has_value();}

    [[nodiscard]] const ObjectId<OT,CM>& value () const {
      return m_id.value();}

    [[nodiscard]] const ObjectId<OT,CM>& operator * () const {
      return m_id.value();}

    [[nodiscard]] StatusCode fillIfEmpty (const ReadObjectHandle<OT,CM>& readObjectHandle)
    {
      if (m_id.has_value())
        return StatusCode::SUCCESS;
      m_id.emplace (readObjectHandle.getSingleObject());
      return StatusCode::SUCCESS;
    }

    [[nodiscard]] auto *getObject () const noexcept {
      return m_id ? m_id->getObject() : nullptr;}

    OptObjectId (const ObjectId<OT,CM>& val_id) noexcept
      : m_id (val_id)
    {}



    /// Private Members
    /// ===============

  private:

    /// the contained ID
    std::optional<ObjectId<OT,CM>> m_id;
  };





  template<ObjectType OT> class OptObjectId<OT,1> final
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr unsigned CM = 1u;

    explicit OptObjectId (std::size_t val_index) noexcept
      : m_id (val_index)
    {}

    [[nodiscard]] operator bool () const noexcept {
      return true;}

    [[nodiscard]] bool has_value () const noexcept {
      return true;}

    [[nodiscard]] const ObjectId<OT,CM>& value () const {
      return m_id;}

    [[nodiscard]] const ObjectId<OT,CM>& operator * () const {
      return m_id;}

    [[nodiscard]] StatusCode fillIfEmpty (const ReadObjectHandle<OT,CM>& /*readObjectHandle*/) {
      return StatusCode::SUCCESS;}

    OptObjectId (const ObjectId<OT,CM>& val_id) noexcept
      : m_id (val_id)
    {}



    /// Private Members
    /// ===============

  private:

    /// the contained ID
    ObjectId<OT,CM> m_id;
  };





#if COLUMNAR_ACCESS_MODE == 0u
#define COL_OPT_NULL = {}
#else
#define COL_OPT_NULL
#endif

  using OptEventId = OptObjectId<ObjectType::event>;
  using OptTrackId = OptObjectId<ObjectType::track>;
}

#endif