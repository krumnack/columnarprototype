/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_CLUSTER_LINK_COLUMN_HANDLE_H
#define COLUMNAR_PROTOTYPE_CLUSTER_LINK_COLUMN_HANDLE_H

#include <AthContainers/AuxElement.h>
#include "AthLinks/ElementLink.h"
#include <ColumnarPrototype/ObjectId.h>
#include <ColumnarPrototype/ObjectClusterLink.h>
#include <ColumnarPrototype/ObjectType.h>
#include <ColumnarPrototype/ReadObjectHandle.h>

#include <Eigen/Dense>

namespace col
{
  template<ObjectType OT,ObjectType LT,unsigned CM = columnarAccessMode> class ClusterLinkColumnHandle;





  template<ObjectType OT,ObjectType LT> class ClusterLinkColumnHandle<OT,LT,0u> final
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr unsigned CM = 0u;

    ClusterLinkColumnHandle (ColumnBaseImp<CM>& /*columnBase*/,
                             const std::string& name)
      : m_accessor(name)
    {
    }

    ObjectClusterLink<LT,CM> operator [] (ObjectId<OT,false,CM> id) const noexcept
    {
      return ObjectClusterLink<LT,CM> (&m_accessor(*id.getObject()));
    }



    /// Private Members
    /// ===============

  private:

    typename SG::AuxElement::ConstAccessor<std::vector<ElementLink<typename detail::ObjectTypeTraits<LT>::xAODContainer>>> m_accessor;
  };





  template<ObjectType OT,ObjectType LT> class ClusterLinkColumnHandle<OT,LT,1u> final
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr unsigned CM = 1u;

    ClusterLinkColumnHandle (ColumnBaseImp<CM>& columnBase,
                             const std::string& name)
    {
      columnBase.addColumn (columnBase.objectName(OT) + "_" + name + "_offset", m_offsetSize, m_offset, columnBase.objectName(OT), 1u);
      columnBase.addColumn (columnBase.objectName(OT) + "_" + name + "_data", m_dataSize, m_data, columnBase.objectName(OT) + "_" + name + "_offset");
    }


    ObjectClusterLink<LT,CM> operator [] (ObjectId<OT,false,CM> id) const noexcept
    {
      return ObjectClusterLink<LT,CM> (m_offset[id.getIndex()+1]-m_offset[id.getIndex()], &m_data[m_offset[id.getIndex()]]);
    }



    /// Private Members
    /// ===============

  private:

    ColumnarOffsetType m_offsetSize = 0u;
    ColumnarOffsetType *m_offset = nullptr;
    ColumnarOffsetType m_dataSize = 0u;
    ColumnarOffsetType *m_data = nullptr;
  };
}

#endif
