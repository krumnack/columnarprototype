/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_COLUMN_BASE_H
#define COLUMNAR_PROTOTYPE_COLUMN_BASE_H

#include <optional>
#include <stdexcept>
#include <string>
#include <unordered_map>

#include <ColumnarPrototype/ObjectType.h>

#include <AsgTools/SgTEvent.h>

#include <AsgMessaging/StatusCode.h>

namespace SG
{
  class VarHandleKey;
}

namespace col
{
  // TO DO: This type still needs to be adjusted to match whatever
  // uproot uses for its offset maps.
  using ColumnarOffsetType = std::size_t;


  /// @brief the base class for all columnar components
  ///
  /// All components (and non-component classes) that want to declare columnar
  /// handles, etc. should inherit from this class.  It contains whatever is
  /// needed to register the data handles, etc.

  template<unsigned CM = columnarAccessMode> class ColumnBaseImp;
  using ColumnBase=ColumnBaseImp<columnarAccessMode>;

  template<> class ColumnBaseImp<0>
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr bool singleEvent = true;

    ColumnBaseImp ();
    virtual ~ColumnBaseImp ();


    /// @brief initialize the columns/column handles
    ///
    /// This should be called at the end of initialize after all data handles
    /// have been declared.
    StatusCode initializeColumns ();



    /// Mode-Specific Public Members
    /// ============================
  public:

    const asg::SgTEvent& eventStore () const noexcept {
      return m_eventStore;}
    void addKey (SG::VarHandleKey *key) {
      m_keys.push_back (key);}



    /// Private Members
    /// ===============
  private:

    asg::SgTEvent m_eventStore;
    std::vector<SG::VarHandleKey*> m_keys;
  };





  template<> class ColumnBaseImp<1>
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr bool singleEvent = false;

    ColumnBaseImp ();
    virtual ~ColumnBaseImp ();

    /// @brief initialize the columns/column handles
    ///
    /// This should be called at the end of initialize after all data handles
    /// have been declared.
    StatusCode initializeColumns ();



    /// Mode-Specific Public Members
    /// ============================
  public:

    const std::string& objectName (ObjectType objectType) const;
    void setObjectName (ObjectType objectType, const std::string& name);

    template<typename CT>
    void addColumn (const std::string& name, std::size_t& size, CT*& dataPtr, const std::string& offsetName, std::size_t extraMembers = 0u) {
      if (columns.find (name) != columns.end())
        throw std::runtime_error ("duplicate column name: " + name);
      ColumnData data;
      data.sizePtr = &size;
      data.dataPtr = &reinterpret_cast<const void*&>(const_cast<const CT*&>(dataPtr));
      data.type = &typeid (std::decay_t<CT>);
      data.isConst = std::is_const_v<CT>;
      data.extraMembers = extraMembers;
      if (!offsetName.empty())
      {
        auto iter = columns.find (offsetName);
        if (iter == columns.end())
          throw std::logic_error ("unknown offset name: " + offsetName);
        if (*iter->second.type != typeid (ColumnarOffsetType))
          throw std::logic_error ("not an offset map: " + offsetName);
        data.offsets = &*iter;
      }
      columns.emplace (name, std::move (data));
    }

    template<typename CT>
    void setColumn (const std::string& name, std::size_t size, CT* dataPtr) {
      auto voidPtr = reinterpret_cast<const void*>(const_cast<const CT*>(dataPtr));
      setColumnVoid (name, size, voidPtr, typeid (std::decay_t<CT>), std::is_const_v<CT>);
    }

    void setColumnVoid (const std::string& name, std::size_t size, const void *dataPtr, const std::type_info& type, bool isConst) {
      auto column = columns.find (name);
      if (column == columns.end())
        throw std::runtime_error ("unknown column name: " + name);

      if (type != *column->second.type)
        throw std::runtime_error ("invalid type for column: " + name);
      if (isConst && !column->second.isConst)
        throw std::runtime_error ("assigning const vector to a non-const column: " + name);
      *column->second.sizePtr = size;
      *column->second.dataPtr = dataPtr;
    }

    void checkColumnsValid () const;

    template<typename CT> std::pair<std::size_t,CT*>
    getColumn (const std::string& name)
    {
      auto [size, ptr] = getColumnVoid (name, &typeid (std::decay_t<CT>), std::is_const_v<CT>);
      if constexpr (std::is_const_v<CT>)
        return std::make_pair (size, static_cast<CT*>(ptr));
      else
        return std::make_pair (size, static_cast<CT*>(const_cast<void*>(ptr)));
    }

    std::pair<std::size_t,const void*>
    getColumnVoid (const std::string& name, const std::type_info *type,
                   bool isConst)
    {
      auto column = columns.find (name);
      if (column == columns.end())
        throw std::runtime_error ("unknown column name: " + name);

      if (*type != *column->second.type)
        throw std::runtime_error ("invalid type for column: " + name);
      if (!isConst && column->second.isConst)
        throw std::runtime_error ("retrieving non-const vector from a const column: " + name);
      if (*column->second.dataPtr != nullptr)
        return std::make_pair (*column->second.sizePtr,
                               *column->second.dataPtr);
      else
        return std::make_pair (0u, nullptr);
    }

    std::vector<std::string> getColumnNames () const;



    /// Private Members
    /// ===============
  private:

    struct ColumnData final
    {
      std::size_t *sizePtr = nullptr;
      const void **dataPtr = nullptr;
      const std::type_info *type = nullptr;
      bool isConst = false;
      std::size_t extraMembers = 0u;
      std::pair<const std::string,const ColumnData> *offsets = nullptr;
    };
    std::unordered_map<std::string,const ColumnData> columns;
    std::unordered_map<ObjectType,std::string> m_objectNames;
  };
}

#endif
