/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_READ_OBJECT_HANDLE_H
#define COLUMNAR_PROTOTYPE_READ_OBJECT_HANDLE_H

#include <ColumnarPrototype/ColumnBase.h>
#include <ColumnarPrototype/ObjectId.h>
#include <ColumnarPrototype/ObjectRange.h>
#include <ColumnarPrototype/ObjectType.h>
#include <assert.h>

#include <AsgDataHandles/ReadHandleKey.h>

namespace col
{
  /// @brief a class for read access to an object in the event store
  template<ObjectType O,unsigned CM=columnarAccessMode> class ReadObjectHandle;




  template<ObjectType O> class ReadObjectHandle<O,0> final
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr unsigned CM = 0u;

    ReadObjectHandle (ColumnBaseImp<CM>& columnBase,
                      const std::string& name)
      : m_columnBase (&columnBase),
        m_key (name)
    {
      columnBase.addKey (&m_key);
    }

    template<typename U>
    ReadObjectHandle (U& owner,
                      const std::string& name,
                      const std::string& propertyName, const std::string& propertyTitle)
      : ReadObjectHandle (owner, name)
    {
      owner.declareProperty (propertyName, m_key, propertyTitle);
    }

    ObjectId<O,false,CM> getSingleObject () const
    {
      static_assert (O == ObjectType::event, "only supported for EventInfo handles");
      const typename detail::ObjectTypeTraits<O>::xAODObject *object = nullptr;
      if (!m_columnBase->eventStore().retrieve (object, name()).isSuccess())
        throw std::runtime_error ("failed to retrieve " + name());
      return ObjectId<O,false,CM> (object);
    }

    ObjectRange<O,false,CM> getFullRange () const
    {
      const typename detail::ObjectTypeTraits<O>::xAODContainer *container = nullptr;
      if (!m_columnBase->eventStore().retrieve (container, name()).isSuccess())
        throw std::runtime_error ("failed to retrieve " + name());
      return ObjectRange<O,false,CM> (container);
    }

    ObjectRange<O,false,CM> getRange (ObjectId<ObjectType::event,false,CM> /*eventId*/) const
    {
      return getFullRange ();
    }

    ObjectRange<O,false,CM> getRange (ObjectRange<ObjectType::event,false,CM> eventRange) const
    {
      if (eventRange.beginIndex() == eventRange.endIndex())
        return ObjectRange<O> (getFullRange().getContainer(), 0u, 0u);
      return getFullRange ();
    }

    const std::string& name () const noexcept
    {
      return m_key.key();
    }

    ColumnBaseImp<CM> *columnBase () noexcept {
      return m_columnBase;}



    /// Private Members
    /// ===============
  private:

    ColumnBaseImp<CM> *m_columnBase = nullptr;
    SG::ReadHandleKey<typename detail::ObjectTypeTraits<O>::xAODStoreType> m_key;
  };






  template<ObjectType O> class ReadObjectHandle<O,1> final
  {
    /// Common Public Members
    /// =====================
  public:

    static constexpr unsigned CM = 1u;

    ReadObjectHandle (ColumnBaseImp<CM>& columnBase,
                      const std::string& name)
      : m_columnBase (&columnBase),
        m_name (name)
    {
      columnBase.setObjectName (O, name);
      columnBase.addColumn (name, m_size, m_offsets, "");
    }

    template<typename U>
    ReadObjectHandle (U& owner,
                      const std::string& name,
                      const std::string& propertyName, const std::string& propertyTitle)
      : ReadObjectHandle (owner, name)
    {
    }

    ObjectRange<O,false,CM> getFullRange () const
    {
      return ObjectRange<O,false,CM> (0u, m_offsets[m_size-1]);
    }

    ObjectRange<O,false,CM> getRange (ObjectId<ObjectType::event,false,CM> eventId) const
    {
      assert (eventId.getIndex() < m_size);
      return ObjectRange<O,false,CM> (m_offsets[eventId.getIndex()], m_offsets[eventId.getIndex() + 1]);
    }

    ObjectRange<O,false,CM> getRange (ObjectRange<ObjectType::event,false,CM> eventRange) const
    {
      assert (eventRange.endIndex() <= m_size);
      return ObjectRange<O,false,CM> (m_offsets[eventRange.beginIndex()], m_offsets[eventRange.endIndex()]);
    }

    const std::string& name () const noexcept
    {
      return m_name;
    }

    const ColumnarOffsetType *const* offsetsPtr () const noexcept {
      return &m_offsets;}

    ColumnBaseImp<CM> *columnBase () noexcept {
      return m_columnBase;}



    /// Private Members
    /// ===============
  private:

    ColumnBaseImp<CM> *m_columnBase = nullptr;

    std::string m_name;
    std::size_t m_size = 0u;
    ColumnarOffsetType *m_offsets = nullptr;
  };





  using EventHandle = ReadObjectHandle<ObjectType::event>;
  using MuonHandle = ReadObjectHandle<ObjectType::muon>;
  using ElectronHandle = ReadObjectHandle<ObjectType::electron>;
  using ClusterHandle = ReadObjectHandle<ObjectType::cluster>;
}

#endif
