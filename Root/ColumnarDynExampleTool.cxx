/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack

//
// includes
//

#include <ColumnarPrototype/ColumnarDynExampleTool.h>

#include <cmath>

//
// method implementations
//

namespace col
{
  ColumnarDynExampleTool ::
  ColumnarDynExampleTool ([[maybe_unused]] const std::string& name)
    : AsgTool (name)
  {}



  StatusCode ColumnarDynExampleTool ::
  initialize ()
  {
    if (!m_input2Name.value().empty())
      m_input2.emplace (*this, m_input2Name.value());
    ANA_CHECK (initializeColumns());
    return StatusCode::SUCCESS;
  }



  void ColumnarDynExampleTool ::
  calculateObject (ObjectId<ObjectType::muon> muon)
  {
    m_output[muon] = m_input1[muon];
    if (m_input2.has_value())
      m_output[muon] += m_input2.value()[muon];
  }



  void ColumnarDynExampleTool ::
  executeObjectLoop ()
  {
    for (ObjectId<ObjectType::muon> muon : m_muons.getFullRange())
      calculateObject (muon);
  }



  void ColumnarDynExampleTool ::
  executeEventsLoop ()
  {
    for (ObjectId<ObjectType::event> event : m_events.getFullRange())
    {
      for (ObjectId<ObjectType::muon> muon : m_muons.getRange(event))
        calculateObject (muon);
    }
  }
}
