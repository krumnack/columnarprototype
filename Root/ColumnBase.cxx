/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack

//
// includes
//

#include <ColumnarPrototype/ColumnBase.h>

#include <AsgDataHandles/VarHandleKey.h>

//
// method implementations
//

namespace col
{
  ColumnBaseImp<0> ::
  ColumnBaseImp ()
  {
  }

  ColumnBaseImp<0> ::
  ~ColumnBaseImp ()
  {
  }

  StatusCode ColumnBaseImp<0> ::
  initializeColumns ()
  {
    for (auto *key : m_keys)
    {
      if (key->initialize().isFailure())
        return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
  }

  ColumnBaseImp<1> ::
  ColumnBaseImp ()
  {
  }

  ColumnBaseImp<1> ::
  ~ColumnBaseImp ()
  {
  }

  StatusCode ColumnBaseImp<1> ::
  initializeColumns ()
  {
    return StatusCode::SUCCESS;
  }



  const std::string& ColumnBaseImp<1> ::
  objectName (ObjectType objectType) const
  {
    auto iter = m_objectNames.find (objectType);
    if (iter == m_objectNames.end())
      throw std::runtime_error ("object type not registered, make sure to register object handle first: " + std::to_string (unsigned(objectType)));
    return iter->second;
  }



  void ColumnBaseImp<1> ::
  setObjectName (ObjectType objectType, const std::string& name)
  {
    auto [iter, success] = m_objectNames.emplace (objectType, name);
    if (!success && iter->second != name)
      throw std::runtime_error ("object type already registered with different name: " + name + " vs " + iter->second);
  }

  void ColumnBaseImp<1> ::
  checkColumnsValid () const
  {
    for (auto& column : columns)
    {
      if (*column.second.dataPtr == nullptr)
        throw std::runtime_error ("uninitialized column: " + column.first);
    }
    std::optional<std::size_t> eventCount;
    for (auto& column : columns)
    {
      if (column.second.offsets != nullptr)
      {
        auto& offsets = *column.second.offsets;
        if (*offsets.second.sizePtr == 0u)
          throw std::runtime_error ("empty offset column: " + offsets.first);
        auto *offsetsPtr = static_cast<const ColumnarOffsetType*>
          (*offsets.second.dataPtr);
        if (*column.second.sizePtr != offsetsPtr[*offsets.second.sizePtr-1] + column.second.extraMembers)
          throw std::runtime_error ("column size doesn't match offset column: " + column.first);
      } else
      {
        if (column.first == "EventInfo")
        {
          if (*column.second.sizePtr != 1u)
            throw std::runtime_error ("EventInfo offset column needs to be exactly length 1");
          if (!eventCount.has_value())
            eventCount = static_cast<const ColumnarOffsetType*> (*column.second.dataPtr)[0];
          else if (eventCount.value() != static_cast<const ColumnarOffsetType*> (*column.second.dataPtr)[0])
            throw std::runtime_error ("inconsistent size for EventInfo offset column");
        } else
        {
          if (!eventCount.has_value())
            eventCount = *column.second.sizePtr;
          else if (eventCount.value() != *column.second.sizePtr + column.second.extraMembers)
            throw std::runtime_error ("inconsistent size for columns without offsets");
        }
      }
    }
  }



  std::vector<std::string> ColumnBaseImp<1> ::
  getColumnNames () const
  {
    std::vector<std::string> result;
    for (auto& column : columns)
      result.push_back (column.first);
    return result;
  }
}
