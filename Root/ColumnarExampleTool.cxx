/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack

//
// includes
//

#include <ColumnarPrototype/ColumnarExampleTool.h>

#include <cmath>

//
// method implementations
//

namespace col
{
  ColumnarExampleTool ::
  ColumnarExampleTool ([[maybe_unused]] const std::string& name)
    : AsgTool (name)
  {}



  StatusCode ColumnarExampleTool ::
  initialize ()
  {
    ANA_CHECK (initializeColumns());
    return StatusCode::SUCCESS;
  }



  void ColumnarExampleTool ::
  calculateObject (ObjectId<ObjectType::muon> muon)
  {
    m_output[muon] = m_pt[muon] * cosh(m_eta[muon]);
  }



  void ColumnarExampleTool ::
  calculateRange (ObjectRange<ObjectType::muon> muons)
  {
    for (ObjectId<ObjectType::muon> muon : muons)
      m_output[muon] = m_pt[muon] * cosh(m_eta[muon]);
  }



  void ColumnarExampleTool ::
  calculateVector (ObjectRange<ObjectType::muon> muons)
  {
    auto pt = m_pt[muons];
    auto eta = m_eta[muons];
    auto output = m_output[muons];
    for (decltype(pt)::Index index = 0; index != pt.size(); ++ index)
      output[index] = pt[index] * cosh(eta[index]);
  }



  void ColumnarExampleTool ::
  executeObjectLoop ()
  {
    for (ObjectId<ObjectType::muon> muon : m_muons.getFullRange())
      calculateObject (muon);
  }

  void ColumnarExampleTool ::
  executeRange ()
  {
    calculateRange (m_muons.getFullRange());
  }

  void ColumnarExampleTool ::
  executeVector ()
  {
    calculateVector (m_muons.getFullRange());
  }



  void ColumnarExampleTool ::
  executeEventsLoop ()
  {
    for (ObjectId<ObjectType::event> event : m_events.getFullRange())
    {
      calculateRange (m_muons.getRange(event));
    }
  }

  void ColumnarExampleTool ::
  executeEventsRange ()
  {
    calculateRange (m_muons.getRange(m_events.getFullRange()));
  }
}
